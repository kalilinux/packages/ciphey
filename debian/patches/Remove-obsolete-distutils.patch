From: Sophie Brun <sophie@offensive-security.com>
Date: Thu, 23 Jan 2025 17:31:51 +0100
Subject: Remove obsolete distutils

Distutils is not longer in 3.12
distutils.utils.strtobool has no equivalent, copy the small function in
ciphey
---
 ciphey/basemods/Crackers/caesar.py    |  5 ++---
 ciphey/basemods/Crackers/vigenere.py  |  5 ++---
 ciphey/basemods/Searchers/ausearch.py |  8 ++++----
 ciphey/common.py                      | 16 ++++++++++++++++
 4 files changed, 24 insertions(+), 10 deletions(-)

diff --git a/ciphey/basemods/Crackers/caesar.py b/ciphey/basemods/Crackers/caesar.py
index 1c74f23..db586d1 100644
--- a/ciphey/basemods/Crackers/caesar.py
+++ b/ciphey/basemods/Crackers/caesar.py
@@ -7,13 +7,12 @@
 © Brandon Skerritt
 Github: brandonskerritt
 """
-from distutils import util
 from typing import Dict, List, Optional, Union
 
 import cipheycore
 from loguru import logger
 
-from ciphey.common import fix_case
+from ciphey.common import fix_case, strtobool
 from ciphey.iface import Config, Cracker, CrackInfo, CrackResult, ParamSpec, registry
 
 
@@ -111,7 +110,7 @@ class Caesar(Cracker[str]):
         super().__init__(config)
         self.lower: Union[str, bool] = self._params()["lower"]
         if not isinstance(self.lower, bool):
-            self.lower = util.strtobool(self.lower)
+            self.lower = strtobool(self.lower)
         self.group = list(self._params()["group"])
         self.expected = config.get_resource(self._params()["expected"])
         self.cache = config.cache
diff --git a/ciphey/basemods/Crackers/vigenere.py b/ciphey/basemods/Crackers/vigenere.py
index a6ef141..d324744 100644
--- a/ciphey/basemods/Crackers/vigenere.py
+++ b/ciphey/basemods/Crackers/vigenere.py
@@ -7,13 +7,12 @@
 © Brandon Skerritt
 Github: brandonskerritt
 """
-from distutils import util
 from typing import Dict, List, Optional, Union
 
 import cipheycore
 from loguru import logger
 
-from ciphey.common import fix_case
+from ciphey.common import fix_case, strtobool
 from ciphey.iface import Config, Cracker, CrackInfo, CrackResult, ParamSpec, registry
 
 
@@ -195,7 +194,7 @@ class Vigenere(Cracker[str]):
         super().__init__(config)
         self.lower: Union[str, bool] = self._params()["lower"]
         if not isinstance(self.lower, bool):
-            self.lower = util.strtobool(self.lower)
+            self.lower = strtobool(self.lower)
         self.group = list(self._params()["group"])
         self.expected = config.get_resource(self._params()["expected"])
         self.cache = config.cache
diff --git a/ciphey/basemods/Searchers/ausearch.py b/ciphey/basemods/Searchers/ausearch.py
index 0f60015..284e8ed 100644
--- a/ciphey/basemods/Searchers/ausearch.py
+++ b/ciphey/basemods/Searchers/ausearch.py
@@ -1,5 +1,4 @@
 import bisect
-import distutils
 import math
 from copy import copy
 from dataclasses import dataclass
@@ -9,6 +8,7 @@ from typing import Any, Dict, Generic, List, Optional, TypeVar, Union
 import cipheycore
 from loguru import logger
 
+from ciphey.common import strtobool
 from ciphey.iface import (
     Checker,
     Config,
@@ -293,11 +293,11 @@ class AuSearch(Searcher):
         self._checker: Checker = config.objs["checker"]
         self.work = PriorityWorkQueue()  # Has to be defined here because of sharing
         self.invert_priority = bool(
-            distutils.util.strtobool(self._params()["invert_priority"])
+            strtobool(self._params()["invert_priority"])
         )
         self.priority_cap = int(self._params()["priority_cap"])
         self.enable_nested = bool(
-            distutils.util.strtobool(self._params()["enable_nested"])
+            strtobool(self._params()["enable_nested"])
         )
         self.max_cipher_depth = int(self._params()["max_cipher_depth"])
         if self.max_cipher_depth == 0:
@@ -337,4 +337,4 @@ class AuSearch(Searcher):
                 desc="Sets the maximum depth before we give up ordering items.",
                 default="2",
             ),
-        }
\ No newline at end of file
+        }
diff --git a/ciphey/common.py b/ciphey/common.py
index 02f6dcc..26ff589 100644
--- a/ciphey/common.py
+++ b/ciphey/common.py
@@ -23,3 +23,19 @@ def fix_case(target: str, base: str) -> str:
             for i in range(len(target))
         ]
     )
+
+def strtobool(val):
+    """Convert a string representation of truth to true (1) or false (0).
+
+    True values are 'y', 'yes', 't', 'true', 'on', and '1'; false values
+    are 'n', 'no', 'f', 'false', 'off', and '0'.  Raises ValueError if
+    'val' is anything else.
+    """
+    val = val.lower()
+    if val in ('y', 'yes', 't', 'true', 'on', '1'):
+        return 1
+    elif val in ('n', 'no', 'f', 'false', 'off', '0'):
+        return 0
+    else:
+        raise ValueError("invalid truth value {!r}".format(val))
+
